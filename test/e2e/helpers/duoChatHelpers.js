import { browser } from '@wdio/globals';

/**
 * Waits for the last Duo Chat response to contain expectedText
 *
 * @async
 * @param {string} expectedText - Text we expect to be included in the response from duo chat
 * @returns {Promise<void>}
 */
const verifyDuoChatResponse = async expectedText => {
  const DUO_CHAT_RESPONSES_LOCATOR = '#chat-component .duo-chat-message';

  let actualText;
  let duoChatResponses;

  await browser.waitUntil(
    async () => {
      duoChatResponses = await browser.$$(DUO_CHAT_RESPONSES_LOCATOR);

      actualText = await duoChatResponses[duoChatResponses.length - 1].getText();

      return actualText.includes(expectedText);
    },
    {
      timeout: 10000,
      timeoutMsg: `Expected "${expectedText}" to be included in last Duo Chat response.`,
    },
  );
};

export { verifyDuoChatResponse };
